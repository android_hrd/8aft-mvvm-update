package com.chhaya.mvvmafternoon.views.article;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.chhaya.mvvmafternoon.R;
import com.chhaya.mvvmafternoon.models.api.entities.Article;
import com.chhaya.mvvmafternoon.models.api.responses.SingleArticleResponse;
import com.chhaya.mvvmafternoon.viewmodels.AddArticleViewModel;
import com.chhaya.mvvmafternoon.viewmodels.ListArticleViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddArticleActivity extends AppCompatActivity
    implements View.OnClickListener {

    @BindView(R.id.edit_title)
    EditText editTitle;
    @BindView(R.id.edit_description)
    EditText editDesc;
    @BindView(R.id.button_add)
    Button btnAdd;

    private AddArticleViewModel addArticleViewModel;
    private ListArticleViewModel listArticleViewModel;

    private int articleId;
    private Article article;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_article);

        onNewIntent(getIntent());

        ButterKnife.bind(this);

        btnAdd.setOnClickListener(this);

        addArticleViewModel = ViewModelProviders
                .of(this).get(AddArticleViewModel.class);
        addArticleViewModel.setId(articleId);
        addArticleViewModel.init();

        addArticleViewModel.getTriggerData().observe(this, new Observer<SingleArticleResponse>() {
            @Override
            public void onChanged(SingleArticleResponse singleArticleResponse) {
                Log.d("tag change", singleArticleResponse + "");
                article = singleArticleResponse.getData();
                setupArticleForUpdate();
            }
        });

        addArticleViewModel.getArticleById();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_add) {
            // TODO: save article
            String title = editTitle.getText().toString();
            String desc = editDesc.getText().toString();
            String image = "https://jakewharton.github.io/butterknife/static/logo.png";
            if (btnAdd.getText() == "Update") {
                image = "http://110.74.194.124:15011/image-thumbnails/thumbnail-0db49ef3-a0af-4a75-bb8d-fede15d44658.jpeg";
                addArticleViewModel.setArticle(new Article(image, desc, title));
                addArticleViewModel.updateArticleById();
                finish();
            } else {
                Article article = new Article();
                article.setTitle(title);
                article.setDescription(desc);
                article.setImage(image);
                addArticleViewModel.insertArticle(article);
                finish();
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle extras = intent.getExtras();
        if (extras != null) {
            articleId = extras.getInt("id");
        }
    }

    private void setupArticleForUpdate() {
        editTitle.setText(article.getTitle());
        editDesc.setText(article.getDescription());
        btnAdd.setText("Update");
    }

}
