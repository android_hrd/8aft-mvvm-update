package com.chhaya.mvvmafternoon.views.article;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.chhaya.mvvmafternoon.R;
import com.chhaya.mvvmafternoon.RecyclerViewCallback;
import com.chhaya.mvvmafternoon.helpers.PaginationHelper;
import com.chhaya.mvvmafternoon.models.api.entities.Article;
import com.chhaya.mvvmafternoon.models.api.entities.Pagination;
import com.chhaya.mvvmafternoon.models.api.responses.ListArticleResponse;
import com.chhaya.mvvmafternoon.viewmodels.ListArticleViewModel;

public class ListArticleActivity extends AppCompatActivity
        implements RecyclerViewCallback {

    private ListArticleResponse dataSet;
    private RecyclerView rcvListArticle;
    private SwipeRefreshLayout swipeContainer;
    private ListArticleAdapter adapter;
    private LinearLayoutManager layoutManager;

    private ListArticleViewModel listArticleViewModel;

    private int currentPage = 1;
    private boolean isReload = false;
    private int articleId;
    private boolean isStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_article);
        isStart = true;
        rcvListArticle = findViewById(R.id.rcv_list_article);

        setupSwipeRefreshLayout();

        listArticleViewModel = ViewModelProviders.of(this).get(ListArticleViewModel.class);
        listArticleViewModel.init();

        getLiveDataFromViewModel();
        fetchListArticle();

    }

    private void setupRecyclerView() {
        if (adapter == null) {
            adapter = new ListArticleAdapter(this, dataSet.getData());
            layoutManager = new LinearLayoutManager(this);
            rcvListArticle.setLayoutManager(layoutManager);
            rcvListArticle.setAdapter(adapter);
        } else if (isReload) {
            adapter.setDataSet(dataSet.getData());
            adapter.notifyDataSetChanged();
            isReload = false;
        } else {
            adapter.appendDataSet(dataSet.getData());
            adapter.notifyDataSetChanged();
        }

        // TODO: implement recycler view pagination:
        rcvListArticle.addOnScrollListener(new PaginationHelper(layoutManager) {
            @Override
            protected void loadMoreData() {
                currentPage++;
                Log.d("tag page", "Page is " + currentPage);
                listArticleViewModel.getPagination().setPage(currentPage);
                fetchListArticle();
            }

            @Override
            public boolean isLastPage() {
                return currentPage == listArticleViewModel.getPagination().getTotalPages();
            }

            @Override
            public boolean isLoading() {
                return swipeContainer.isRefreshing();
            }
        });

    }

    private void setupSwipeRefreshLayout() {
        swipeContainer = findViewById(R.id.swipe_container);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // TODO: reload data
                isReload = true;
                listArticleViewModel.getPagination().setPage(PaginationHelper.PAGE_NUM);
                fetchListArticle();
            }
        });
    }

    // Subscribe to view model and update ui
    private void getLiveDataFromViewModel() {
        swipeContainer.setRefreshing(true);
        listArticleViewModel.getLiveData().observe(this, new Observer<ListArticleResponse>() {
            @Override
            public void onChanged(ListArticleResponse listArticleResponse) {
                Log.d("tag change", "onChange");
                dataSet = listArticleResponse;
                setupRecyclerView();
                Log.d("tag change", "Result = " + dataSet.getData());
                swipeContainer.setRefreshing(false);
            }
        });
    }

    //Add Today
    private void fetchListArticle() {
        swipeContainer.setRefreshing(true);
        listArticleViewModel.fetchListArticleByPaging();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu_item, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add: {
                Intent addIntent = new Intent(
                        ListArticleActivity.this,
                        AddArticleActivity.class
                );
                startActivity(addIntent);
                break;
            }
            default:
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!isStart) {
            isReload = true;
            listArticleViewModel.getPagination().setPage(1);
            fetchListArticle();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isStart = false;
    }

    @Override
    public void onItemClick(int position) {
        Log.d("tag click", "Item is clicked");
    }

    @Override
    public void onUpdateClick(int position) {
        Intent updateIntent = new Intent(
                ListArticleActivity.this,
                AddArticleActivity.class
        );
        articleId = dataSet.getData().get(position).getId();
        Log.e("tag update", articleId + "");
        updateIntent.putExtra("id", articleId);
        startActivity(updateIntent);
    }

    @Override
    public void onDeleteClick(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Delete")
                .setMessage("Are you sure to delete?")
                .setPositiveButton("Delete!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        articleId = dataSet.getData().get(position).getId();
                        listArticleViewModel.setId(articleId);
                        listArticleViewModel.deleteArticleById();
                        adapter.notifyItemRemoved(position);
                        //listArticleViewModel.getPagination().setPage(currentPage);
                        //fetchListArticle();
                        //Log.d("tag delete", dataSet.getData().get(position).getTitle());
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // cancel delete
                    }
                });
        builder.create();
        builder.show();
    }
}
