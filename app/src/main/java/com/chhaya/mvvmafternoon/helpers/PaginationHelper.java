package com.chhaya.mvvmafternoon.helpers;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class PaginationHelper extends RecyclerView.OnScrollListener {

    public final static int PAGE_NUM = 1;
    public final static int PAGE_LIMIT = 15;

    private LinearLayoutManager layoutManager;

    public PaginationHelper(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int itemCount = layoutManager.getItemCount();
        int visibleItemCount = layoutManager.getChildCount();
        int firstVisibleItemCount = layoutManager.findFirstVisibleItemPosition();

        if (!isLastPage() && !isLoading()) {
            if ((visibleItemCount + firstVisibleItemCount) >= itemCount
                    && firstVisibleItemCount >= 0
                    && itemCount >= PAGE_LIMIT) {

                // TODO: call method to load more data
                loadMoreData();

            }
        }

    }

    protected abstract void loadMoreData();
    public abstract boolean isLastPage();
    public abstract boolean isLoading();
}
