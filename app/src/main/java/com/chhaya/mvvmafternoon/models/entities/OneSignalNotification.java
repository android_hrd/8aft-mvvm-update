package com.chhaya.mvvmafternoon.models.entities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OneSignalNotification {

    private String appId;
    private JSONArray playerIds;
    private JSONObject headings;
    private JSONObject contents;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public JSONArray getPlayerIds() {
        return playerIds;
    }

    public void setPlayerIds(JSONArray playerIds) {
        this.playerIds = playerIds;
    }

    public JSONObject getHeadings() {
        return headings;
    }

    public void setHeadings(JSONObject headings) {
        this.headings = headings;
    }

    public JSONObject getContents() {
        return contents;
    }

    public void setContents(JSONObject contents) {
        this.contents = contents;
    }

    public JSONObject toJsonObject() {
        JSONObject object = new JSONObject();

        try {
            object.put("app_id", getAppId());
            object.put("include_player_ids", getPlayerIds());
            object.put("headings", getHeadings());
            object.put("contents", getContents());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return object;
    }

}
