package com.chhaya.mvvmafternoon.models.api.services;

import com.chhaya.mvvmafternoon.models.api.entities.Article;
import com.chhaya.mvvmafternoon.models.api.responses.ListArticleResponse;
import com.chhaya.mvvmafternoon.models.api.responses.SingleArticleResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {

    @GET("v1/api/articles")
    Call<ListArticleResponse> getAll(@Query("page") long page,
                                     @Query("limit") long limit);

    @GET("v1/api/articles/{id}")
    Call<SingleArticleResponse> getOne(@Path("id") int id);

    @POST("v1/api/articles")
    Call<SingleArticleResponse> insertOne(@Body Article article);

    @DELETE("v1/api/articles/{id}")
    Call<SingleArticleResponse> deleteOne(@Path("id") int id);

    @PUT("v1/api/articles/{id}")
    Call<SingleArticleResponse> updateOne(@Path("id") int id, @Body Article article);

}
