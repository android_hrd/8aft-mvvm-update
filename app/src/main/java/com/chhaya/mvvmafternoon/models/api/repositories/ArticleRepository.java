package com.chhaya.mvvmafternoon.models.api.repositories;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.chhaya.mvvmafternoon.models.api.config.ServiceGenerator;
import com.chhaya.mvvmafternoon.models.api.entities.Article;
import com.chhaya.mvvmafternoon.models.api.entities.Pagination;
import com.chhaya.mvvmafternoon.models.api.responses.ListArticleResponse;
import com.chhaya.mvvmafternoon.models.api.responses.SingleArticleResponse;
import com.chhaya.mvvmafternoon.models.api.services.ArticleService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {

    private ArticleService articleService;

    public ArticleRepository() {
        articleService = ServiceGenerator.createService(ArticleService.class);
    }

    public MutableLiveData<ListArticleResponse> getListArticleByPagination(Pagination pagination) {

        final MutableLiveData<ListArticleResponse> liveData = new MutableLiveData<>();

        // TODO implement retrofit
        Call<ListArticleResponse> call = articleService.getAll(pagination.getPage(), pagination.getLimit());

        call.enqueue(new Callback<ListArticleResponse>() {
            @Override
            public void onResponse(Call<ListArticleResponse> call, Response<ListArticleResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("tag success", response.body().getMessage());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListArticleResponse> call, Throwable t) {
                Log.e("tag error", t.getMessage());
            }
        });


        return liveData;

    }

    // Implement logic to add article
    public MutableLiveData<SingleArticleResponse> insertArticle(Article article) {
        final MutableLiveData<SingleArticleResponse> liveData =
                new MutableLiveData<>();

        articleService.insertOne(article).enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("tag succeed", response.body().getMessage());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
                Log.e("tag error", t.getMessage());
            }
        });

        return liveData;
    }

    // Delete article by Id
    public MutableLiveData<SingleArticleResponse> deleteArticleById(int id) {
        final MutableLiveData<SingleArticleResponse> liveData
                = new MutableLiveData<>();

        articleService.deleteOne(id).enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("tag success", response.body().getMessage());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
                Log.d("tag fail", t.getMessage());
            }
        });

        return liveData;
    }

    // Update article by id:
    public MutableLiveData<SingleArticleResponse> updateArticleById(int id, Article article) {

        final MutableLiveData<SingleArticleResponse> liveData
                = new MutableLiveData<>();

        articleService.updateOne(id, article).enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("tag success", response.body().getMessage());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
                Log.d("tag fail", t.getMessage());
            }
        });

        return liveData;
    }

    // Get article by Id
    public MutableLiveData<SingleArticleResponse> getArticleById(int id) {
        final MutableLiveData<SingleArticleResponse> liveData
                = new MutableLiveData<>();

        articleService.getOne(id).enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
                if (response.isSuccessful()) {
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
                Log.e("tag fail", t.getMessage());
            }
        });

        return liveData;
    }


}
