package com.chhaya.mvvmafternoon.models.api.responses;

import com.chhaya.mvvmafternoon.models.api.entities.Article;
import com.google.gson.annotations.SerializedName;

public class SingleArticleResponse {

    @SerializedName("DATA")
    private Article data;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("CODE")
    private String code;

    public SingleArticleResponse() {

    }

    public Article getData() {
        return data;
    }

    public void setData(Article data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
