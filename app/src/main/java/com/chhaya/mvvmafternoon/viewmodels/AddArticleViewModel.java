package com.chhaya.mvvmafternoon.viewmodels;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.chhaya.mvvmafternoon.models.api.entities.Article;
import com.chhaya.mvvmafternoon.models.api.repositories.ArticleRepository;
import com.chhaya.mvvmafternoon.models.api.responses.SingleArticleResponse;

public class AddArticleViewModel extends ViewModel {

    private MutableLiveData<SingleArticleResponse> triggerData;
    private LiveData<SingleArticleResponse> liveData;
    private ArticleRepository articleRepository;
    private int id;
    private Article article;

    public void init() {
        articleRepository = new ArticleRepository();
        triggerData = new MutableLiveData<>();
        liveData = Transformations.switchMap(triggerData, new Function<SingleArticleResponse, LiveData<SingleArticleResponse>>() {
            @Override
            public LiveData<SingleArticleResponse> apply(SingleArticleResponse input) {
                return articleRepository.getArticleById(id);
            }
        });
    }

    public void insertArticle(Article article) {
        triggerData = articleRepository.insertArticle(article);
    }

    public LiveData<SingleArticleResponse> getTriggerData() {
        return liveData;
    }

    public void getArticleById() {
        triggerData.setValue(articleRepository.getArticleById(id).getValue());
    }

    public void updateArticleById() {
        articleRepository.updateArticleById(id, article);
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

}
