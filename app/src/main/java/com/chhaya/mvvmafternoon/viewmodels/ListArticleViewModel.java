package com.chhaya.mvvmafternoon.viewmodels;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.chhaya.mvvmafternoon.models.api.entities.Article;
import com.chhaya.mvvmafternoon.models.api.entities.Pagination;
import com.chhaya.mvvmafternoon.models.api.repositories.ArticleRepository;
import com.chhaya.mvvmafternoon.models.api.responses.ListArticleResponse;
import com.chhaya.mvvmafternoon.models.api.responses.SingleArticleResponse;

public class ListArticleViewModel extends ViewModel {

    private MutableLiveData<ListArticleResponse> triggerData
            = new MutableLiveData<>();
    //Add Today
    private LiveData<ListArticleResponse> liveData;
    private Pagination pagination;
    private int id;

    private ArticleRepository articleRepository;

    public void init() {
        articleRepository = new ArticleRepository();
        pagination = new Pagination();
        liveData = Transformations.switchMap(triggerData, new Function<ListArticleResponse, LiveData<ListArticleResponse>>() {
            @Override
            public LiveData<ListArticleResponse> apply(ListArticleResponse input) {
                return articleRepository.getListArticleByPagination(pagination);
            }
        });
    }

    public LiveData<ListArticleResponse> getLiveData() {
        return liveData;
    }

    public void fetchListArticleByPaging() {
        triggerData.setValue(articleRepository
        .getListArticleByPagination(pagination).getValue());
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void deleteArticleById() {
        articleRepository.deleteArticleById(id);
    }

    public Article getArticleById() {
        return articleRepository.getArticleById(id).getValue().getData();
    }

    public void setId(int id) {
        this.id = id;
    }

}
