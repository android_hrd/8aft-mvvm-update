package com.chhaya.mvvmafternoon;

public interface RecyclerViewCallback {

    void onItemClick(int position);
    void onUpdateClick(int position);
    void onDeleteClick(int position);

}
